package prashant.dd.com.mvpexample.presenter;

import prashant.dd.com.mvpexample.model.User;
import prashant.dd.com.mvpexample.repository.IRepository;
import prashant.dd.com.mvpexample.view.IUserView;

/**
 * Created by prrane on 10/21/16.
 */

public class UserViewPresenter {

    private IUserView pUserView;
    private IRepository pSqlitehandler;
    private User pUser;

    public UserViewPresenter(IRepository sq) {
        pSqlitehandler =  sq;
    }

    public void setView(IUserView view) {
        pUserView = view;
    }

    public void loadUserDetailsFromModel() {
        pUser = pSqlitehandler.getUser(pUserView.getUsername());
        if(pUser!=null) {
            pUserView.showUsername(pUser.getUserName());
            pUserView.showUserLastName(pUser.getUserLastName());
        }
        else{
            pUserView.showerrorMessage("User Not Found");
        }

    }

    public void saveUserDetails() {

        if (pUserView.getUsername().isEmpty()) {
            pUserView.showerrorMessage("Empty user name");
        }
        else if (pUserView.getLastName().isEmpty()) {
            pUserView.showerrorMessage("Empty user name");
        }
        else
        pSqlitehandler.insertUser(new User(pUserView.getUsername(), pUserView.getLastName()));
    }
}
