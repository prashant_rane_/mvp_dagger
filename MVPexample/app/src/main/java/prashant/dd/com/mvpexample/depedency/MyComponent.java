package prashant.dd.com.mvpexample.depedency;

import javax.inject.Singleton;

import prashant.dd.com.mvpexample.view.activity.MainActivity;

/**
 * Created by prrane on 10/21/16.
 */
@Singleton
@dagger.Component(modules = {UserModule.class})
public interface MyComponent {

    void inject(MainActivity activity);
}
