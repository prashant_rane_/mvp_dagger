package prashant.dd.com.mvpexample.JavaTest;

/**
 * Created by prrane on 10/22/16.
 */

public class TryCatch {


        public static int method(){


            try {

                System.out.println("inside try block");

               // return 10;
            } finally{

                System.out.println("inside finally block");
            }

            System.out.println("after finally block");
            return 10;
        }

        public static void main(String[] args) {

            System.out.println(method());

        }

    }

