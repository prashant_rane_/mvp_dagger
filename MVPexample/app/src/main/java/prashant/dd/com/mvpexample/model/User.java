package prashant.dd.com.mvpexample.model;

/**
 * Created by prrane on 10/21/16.
 */
public class User {



    private String userName;
    private String userLastName;

    public User( String name, String lastname) {
        userName=name;
        userLastName=lastname;
    }

    public User()
    {

    }
    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getUserName() {
        return userName;
    }

    public String getUserLastName() {
        return userLastName;
    }
}
