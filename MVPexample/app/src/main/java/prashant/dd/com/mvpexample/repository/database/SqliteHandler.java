package prashant.dd.com.mvpexample.repository.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import prashant.dd.com.mvpexample.model.User;
import prashant.dd.com.mvpexample.repository.IRepository;

/**
 * Created by prrane on 10/21/16.
 */

public class SqliteHandler extends SQLiteOpenHelper implements IRepository{

    //Constructor info
    private static final String DATABASE_NAME = "usertable";
    private static final int DATABASE_VERSION = 1;

    //table info for cursor adapter n all _id is neccessary column
    private static String USER_INFO_TABLE = "user_info";
    private static String USER_NAME = "username";
    private static String USER_LAST_NAME = "userlastname";
    private static String KEY_ID = "ID";

    public SqliteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        //TABLE INFO

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACT_TABLE = "CREATE TABLE " + USER_INFO_TABLE + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                USER_NAME + " TEXT, " +
                USER_LAST_NAME + " TEXT " + ")";
        db.execSQL(CREATE_CONTACT_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_TABLE = "DROP TABLE IF EXIST " + USER_INFO_TABLE;
        db.execSQL(DROP_TABLE);
        onCreate(db);

    }
    @Override
    public void insertUser(User user) {
        ContentValues cv = new ContentValues();
        cv.put(USER_NAME, user.getUserName()); // Contact Name
        cv.put(USER_LAST_NAME, user.getUserLastName()); // Contact Phone

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(USER_INFO_TABLE, null, cv);
        db.close();

    }
    @Override
    public User getUser(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cr;
        try {
            cr = db.query(USER_INFO_TABLE, new String[]{USER_NAME, USER_LAST_NAME}, USER_NAME + "=?", new String[]{name}, null, null, null);

            if (cr.getCount() > 0) {
                cr.moveToNext();
                int i = cr.getColumnIndex(USER_NAME);
                int j = cr.getColumnIndex(USER_LAST_NAME);
                String lname = cr.getString(i);
                String lLastname = cr.getString(j);
                User u = new User(cr.getString(i), cr.getString(j));
                cr.close();
                db.close();
                return u;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
