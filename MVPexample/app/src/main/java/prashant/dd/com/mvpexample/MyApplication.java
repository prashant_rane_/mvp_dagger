package prashant.dd.com.mvpexample;

import android.app.Application;

import prashant.dd.com.mvpexample.depedency.DaggerMyComponent;
import prashant.dd.com.mvpexample.depedency.MyComponent;
import prashant.dd.com.mvpexample.depedency.UserModule;

/**
 * Created by prrane on 10/21/16.
 */

public class MyApplication extends Application {

    private MyComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component= DaggerMyComponent.builder().userModule(new UserModule(this)).build();

    }

    public MyComponent getComponent(){
        return  component;
    }
}
