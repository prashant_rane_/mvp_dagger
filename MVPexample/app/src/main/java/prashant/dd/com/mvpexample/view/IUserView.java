package prashant.dd.com.mvpexample.view;

/**
 * Created by prrane on 10/21/16.
 */

public interface IUserView {

    void showUsername(String name);
    void showUserLastName(String lastname);

    String getUsername();
    String getLastName();

    void showerrorMessage(String message);
}
