package prashant.dd.com.mvpexample.repository;

import prashant.dd.com.mvpexample.model.User;

/**
 * Created by prrane on 10/26/16.
 */

public interface IRepository {
    public void insertUser(User user);
    public User getUser(String name);
}
