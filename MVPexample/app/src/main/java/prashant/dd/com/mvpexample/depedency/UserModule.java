package prashant.dd.com.mvpexample.depedency;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import prashant.dd.com.mvpexample.MyApplication;
import prashant.dd.com.mvpexample.presenter.UserViewPresenter;
import prashant.dd.com.mvpexample.repository.IRepository;
import prashant.dd.com.mvpexample.repository.database.SqliteHandler;

/**
 * Created by prrane on 10/21/16.
 */
@Singleton
@Module
public class UserModule {

    private final Context c;
    public UserModule(MyApplication myApplication) {
        c=myApplication;
    }

    @Singleton
    @Provides
    UserViewPresenter getUseViewPresenter(IRepository sq){
        return new UserViewPresenter(sq);
    }

    @Provides
    IRepository getSqliteHandler(Context app){
        return new SqliteHandler(app);
    }


    @Singleton
    @Provides
    Application getApplication(){
        return new MyApplication();
    }

    @Provides //scope is not necessary for parameters stored within the module
    public Context context() {
        return c;
    }
}
