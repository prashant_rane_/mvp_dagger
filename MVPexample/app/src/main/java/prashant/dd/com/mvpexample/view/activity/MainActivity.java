package prashant.dd.com.mvpexample.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import prashant.dd.com.mvpexample.MyApplication;
import prashant.dd.com.mvpexample.R;
import prashant.dd.com.mvpexample.presenter.UserViewPresenter;
import prashant.dd.com.mvpexample.view.IUserView;

public class MainActivity extends AppCompatActivity implements IUserView{

    private EditText mUsername, mLastname;
    private Button mSubmit,mSave;

    @Inject
    UserViewPresenter mUserviewPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((MyApplication)getApplication()).getComponent().inject(this);
        setUI();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mUserviewPresenter.setView(this);
    }

    private void setUI() {
        mUsername= (EditText) findViewById(R.id.editTextUserName);
        mLastname= (EditText) findViewById(R.id.editTextLastName);
        mSubmit= (Button) findViewById(R.id.buttonSubmit);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserviewPresenter.loadUserDetailsFromModel();
            }
        });
        mSave=(Button)findViewById(R.id.savebutton);
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserviewPresenter.saveUserDetails();
            }
        });

    }


    @Override
    public void showUsername(String name) {
        mUsername.setText(name);
    }

    @Override
    public void showUserLastName(String lastname) {
mLastname.setText(lastname);
    }

    @Override
    public String getUsername() {
        return mUsername.getText().toString();
    }

    @Override
    public String getLastName() {
        return mLastname.getText().toString();
    }

    @Override
    public void showerrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
