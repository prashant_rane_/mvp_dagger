package prashant.dd.com.mvpexample.JavaTest;

/**
 * Created by prrane on 10/22/16.
 */

public class ClassExample {
    // should not do it public
    public int counter=0;

    public class Inner {

        public void printFromInner(){
            System.out.println("Print  outer member from inner");
        }
    }

    public static void main(String[] arg) {
        Abc c = new Abc();
        c.printSome();
    }

    public Inner getInerObject(){
        return new Inner();
    }
}

class Abc {

    public void printSome() {
        System.out.println("Access");
    }
}
