package prashant.dd.com.mvpexample;

import org.junit.Before;
import org.junit.Test;

import prashant.dd.com.mvpexample.model.User;
import prashant.dd.com.mvpexample.presenter.UserViewPresenter;
import prashant.dd.com.mvpexample.repository.IRepository;
import prashant.dd.com.mvpexample.view.IUserView;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by prrane on 10/22/16.
 */

public class UserPresenterTest {

    IRepository mocksqliteHandler;
    User user;
    IUserView mockuserView;
    UserViewPresenter userViewPresenter;

    @Before
    public void setUp() {
        mocksqliteHandler = mock(IRepository.class);

        user = new User();
        user.setUserLastName("rane");
        user.setUserName("prashant");
        when(mocksqliteHandler.getUser("prashant")).thenReturn(user);

        mockuserView= mock(IUserView.class);
        userViewPresenter= new UserViewPresenter(mocksqliteHandler);
        userViewPresenter.setView(mockuserView);

    }

    @Test
    public void shouldbeabletoloadUserInformation(){

        userViewPresenter.loadUserDetailsFromModel();

        //verifyZeroInteractions(mockuserView);
    }

}
